//
//  SearchReviewViewController.swift
//  Reviews
//
//  Created by Данил on 10.04.17.
//  Copyright © 2017 DevingLabs. All rights reserved.
//

import UIKit
import MapKit
import ARSLineProgress

class SearchReviewViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {

    // outlets
    @IBOutlet weak var companyNameTextField: CircleTextField!
    @IBOutlet weak var companyCategoryTextField: CircleTextField!
    @IBOutlet weak var addressTextField: CircleTextField!
    
    // UI vars
    var searchResultsTableView: UITableView!
    var dark: Darkness!
    var activeTextField: UITextField!
    
    // static vars
    var keyboardRect = CGRect()
    var searchResults = SearchResults()
    var selectedCompany: Company? = nil
    
    // static lets
    private let REVIEW_DISABLE_COLOR = UIColor(netHex: 0xf1f1f1)
    
    let locationManager = CLLocationManager()
    
    func initDark() {
        dark = Darkness(alpha: 0.15)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedLocation = (address: "", coordinate: CLLocationCoordinate2D(), country: "", city: "")
        
        initDark() // not add as subview
        initTableView()
        initLocationManager()
        
        delegateTextFields()

        addNotificationsForKeyboard()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        adjustViewControllerToNewReview()
        
        if !selectedLocation.address.isEmpty {
            addressTextField.text = selectedLocation.address
            addressTextField.setRightImage(image: #imageLiteral(resourceName: "success-field"))
        }
    }

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "findReviewsSegue" {
            guard let _ = selectedCompany else {
                return false
            }
        }
        
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "findReviewsSegue" {
            guard let vc = segue.destination as? CompanyViewController, let company = selectedCompany else {
                return
            }
            
            vc.company = company
        } else if segue.identifier == "findOnMapSegue" {
            guard let vc = segue.destination as? MapViewController else {
                return
            }
            
            vc.isNeedToChangeTopLayoutGuide = true
        }
    }


    func hideTableView() {
        guard let _ = searchResultsTableView else {
            return
        }
        
        UIView.animate(withDuration: 0.3, animations: {
            self.searchResultsTableView.alpha = 0
        })
    }
    
    func showTableView(textField: UITextField) {
        guard let _ = searchResultsTableView else {
            return
        }
        self.view.bringSubview(toFront: self.searchResultsTableView)
        
        if self.keyboardRect != CGRect() && !self.activeTextField.text!.isEmpty {
            self.searchResultsTableView.frame.origin.y = self.activeTextField.frame.maxY + 5
            self.searchResultsTableView.frame.size.height = self.keyboardRect.minY - self.activeTextField.frame.maxY - 10 - self.keyboardRect.size.height
            UIView.animate(withDuration: 0.3, animations: {
                self.searchResultsTableView.alpha = 1
            })
        }
    
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return searchResults.type == .company ? UITableViewAutomaticDimension : 44
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return searchResults.type == .company ? 78 : 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if searchResults.type == .country || searchResults.type == .city || searchResults.type == .address {
            let cell = tableView.dequeueReusableCell(withIdentifier: "oneLabelCell", for: indexPath) as! CountryTableViewCell
            cell.configureCellI(text: searchResults.data[indexPath.row] as! String, textUppercased: true)
            return cell
        } else if searchResults.type == .company {
            
            let companies = searchResults.data as! [Company]
            let company = companies[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "companyCell", for: indexPath) as! CompanyTableViewCell
            cell.configureCell(company: company)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if searchResults.type == .company {
            let company = (searchResults.data as! [Company])[indexPath.row]
            adjustViewControllerForCompany(company: company)
        }
        
        mainHide()
    }
    
    func adjustViewControllerToNewReview() {
        let enableColor: UIColor = .white
        addressTextField.backgroundColor = enableColor
        companyCategoryTextField.backgroundColor = enableColor
        companyNameTextField.backgroundColor = enableColor
        
        addressTextField.isEnabled = true
        companyCategoryTextField.isEnabled = true
        self.selectedCompany = nil
    }
    
    func adjustViewControllerForCompany(company: Company) {
        self.selectedCompany = company
        
        addressTextField.backgroundColor = REVIEW_DISABLE_COLOR
        companyCategoryTextField.backgroundColor = REVIEW_DISABLE_COLOR
        companyNameTextField.backgroundColor = REVIEW_DISABLE_COLOR
        
        addressTextField.isUserInteractionEnabled = false
        companyCategoryTextField.isUserInteractionEnabled = false
        
        addressTextField.text = company.address
        companyCategoryTextField.text = company.category
        companyNameTextField.text = company.name
    }
    
    func searchByCountry(text: String) -> [String] {
        var returnedArray: [String] = []
        for country in countries {
            if country.lowercased().contains(text.lowercased()) {
                returnedArray.append(country)
            }
        }
        return returnedArray
    }
    
    func mainHide() {
        dark.hide()
        resignAllTextFields()
        hideTableView()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.data.count
    }

    func addNotificationsForKeyboard() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        delay(delay: 0.2) {
            UIView.animate(withDuration: 0.2, animations: {
                if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                    self.keyboardRect = keyboardSize
                }
            })
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        mainHide()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let updatedText = (textField.text! as NSString).replacingCharacters(in: range, with: string) as String
            activeTextField = textField
        if !updatedText.isEmpty {
            switch activeTextField {
            case companyNameTextField:
                LCFunc().searchByCompany(name: updatedText, category: companyCategoryTextField.text, city: selectedLocation.city, country: selectedLocation.country, completion: { (companies) in
                    
                    self.searchResults.data = companies
                    self.searchResults.type = .company
                    self.searchResultsTableView.reloadData()
                    self.showTableViewIfNeeded()
                })
            case addressTextField:
                LCFunc().searchByStreet(text: updatedText, completion: { (adresses) in
                    self.searchResults.data = adresses
                    self.searchResults.type = .address
                    self.searchResultsTableView.reloadData()
                    self.showTableViewIfNeeded()
                })
                
            default: break
                
            }
            
        } else {
            searchResults.clear()
            hideTableView()
        }
        
        showTableViewIfNeeded()
       
        if searchResults.type != .address || searchResults.type != .company {
            searchResultsTableView.reloadData()
        }
        
        return true
    }
    
    func showTableViewIfNeeded() {
        if searchResults.type == .null || searchResults.data.count == 0 { //hide tableview
            hideTableView()
        } else {
            showTableView(textField: activeTextField)
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField is CircleTextField {
            (textField as! CircleTextField).off()
        }
        
        hideTableView()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == addressTextField {
            textField.resignFirstResponder()
            self.transferVCwith(identifier: "selectMap", method: .push)
        } else {
            if textField is CircleTextField {
                (textField as! CircleTextField).on()
            }
            
            activeTextField = textField
            
            view.addSubview(dark)
            view.bringSubview(toFront: textField)
            dark.show()
            
            //start searching
            LCFunc().searchByCompany(name: "", category: companyCategoryTextField.text, city: selectedLocation.city, country: selectedLocation.country, completion: { (companies) in
                
                self.searchResults.data = companies
                self.searchResults.type = .company
                self.searchResultsTableView.reloadData()
                self.showTableViewIfNeeded()
            })
        
        }
    }
    
    func delegateTextFields() {
        companyNameTextField.delegate = self
        companyCategoryTextField.delegate = self
        addressTextField.delegate = self
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        var isActivated = false
        for subview in view.subviews where subview is UITextField {
            if (subview as! UITextField).isFirstResponder {
                isActivated = true; break
            }
        }
        
        if isActivated && activeTextField == companyNameTextField {
            adjustViewControllerToNewReview()
        }
        
        resignAllTextFields()
        hideTableView()
        dark.hide()
    }
    
    func resignAllTextFields() {
        for textField in view.subviews where textField is CircleTextField {
            textField.resignFirstResponder()
        }
    }
    
    func initTableView() {
        searchResultsTableView = UITableView()
        searchResultsTableView.frame.size = CGSize(width: view.frame.width * 0.8, height: view.frame.height * 0.4)
        searchResultsTableView.center.x = view.center.x
        searchResultsTableView.layer.cornerRadius = 15
        searchResultsTableView.backgroundColor = .white
        searchResultsTableView.embedStandartShadow()
        searchResultsTableView.alpha = 0
        searchResultsTableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 15)
        searchResultsTableView.delegate = self
        searchResultsTableView.dataSource = self
        searchResultsTableView.clipsToBounds = true
        
        searchResultsTableView.register(CountryTableViewCell.self, forCellReuseIdentifier: "oneLabelCell")
        let nib = UINib(nibName: "CompanyTableCellXib", bundle: nil)
        searchResultsTableView.register(nib, forCellReuseIdentifier: "companyCell")
        
        view.addSubview(searchResultsTableView)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationController?.hide()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        navigationController?.show()
        setLeftBarButton()
        
//        navigationController?.navigationBar.isTranslucent = true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

    func setLeftBarButton() {
        let barButton = UIBarButtonItem(image: #imageLiteral(resourceName: "iconBackArrow"), style: .plain, target: self, action: #selector(self.pop(sender:)))
        barButton.tintColor = .black
        navigationItem.leftBarButtonItem = barButton
    }

    func pop(sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initLocationManager() {
        locationManager.delegate = self
        
        if CLLocationManager.authorizationStatus() == .notDetermined {
            locationManager.requestAlwaysAuthorization()
        }
        else if CLLocationManager.authorizationStatus() == .denied {
            showAlert(title: "Error", message: "Location services were previously denied. Please enable location services for this app in Settings.")
        }
        else if CLLocationManager.authorizationStatus() == .authorizedAlways {
            locationManager.startUpdatingLocation()
        }
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
