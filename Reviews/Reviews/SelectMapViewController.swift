//
//  SelectMapViewController.swift
//  Reviews
//
//  Created by Данил on 11.05.17.
//  Copyright © 2017 DevingLabs. All rights reserved.
//

import UIKit
import MapKit
import AddressBookUI
import Alamofire
import SwiftyJSON

public var selectedLocation = (address: "", coordinate: CLLocationCoordinate2D(), country: "", city: "")

class SelectMapViewController: UIViewController, MKMapViewDelegate, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchTableView: UITableView!
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell", for: indexPath)
        cell.textLabel?.text = searchResults[indexPath.row].0
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let coordinates = searchResults[indexPath.row].1
        CLGeocoder().reverseGeocodeLocation(coordinates.toLocation()) { (placemarks, error) in
            if error != nil {
                self.showAlert(title: "Ошибка", message: "Ошибка с определением адреса.")
            } else {
                let placemark = placemarks!.first!
                if placemark.country == nil || placemark.administrativeArea == nil {
                    self.showAlert(title: "Ошибка", message: "На этой локации не доступно определение города или страны, пожалуйста, выберите место поближе к определенной улице.")
                } else {
                    selectedLocation = (address: self.searchResults[indexPath.row].0, coordinate: coordinates, country: placemark.country!, city: placemark.administrativeArea!)
                    self.pop(sender: UIBarButtonItem())
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    
    var searchResults = [(String, CLLocationCoordinate2D)]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self

        let uilgr = UILongPressGestureRecognizer(target: self, action: #selector(self.action(gestureRecognizer:)))
        uilgr.minimumPressDuration = 1.0
        mapView.addGestureRecognizer(uilgr)
        mapView.showsUserLocation = true
        mapView.userTrackingMode = .followWithHeading
        
        addNotificationsForKeyboard()
        
        searchTableView.delegate = self
        searchTableView.dataSource = self
        searchBar.delegate = self
        searchTableView.alpha = 0
        searchBar.alpha = 0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setStartedPositions()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchAddress(text: searchText) { (result: [(String, CLLocationCoordinate2D)]) in
            self.searchResults = result
            self.searchTableView.reloadData()
        }
    }
    
    func setStartedPositions() {
        searchTableView.frame.size.height = 0
        searchTableView.frame.origin.y = view.frame.maxY
        searchBar.frame.origin.y = view.frame.maxY - 44
        
        UIView.animate(withDuration: 0.3, animations: {
            self.searchTableView.alpha = 1
            self.searchBar.alpha = 1
        })
    }
    
    func searchAddress(text: String, result: @escaping (_ arrayOfResults: [(String, CLLocationCoordinate2D)]) -> Void) {
        var geocodeURLString = "https://maps.googleapis.com/maps/api/geocode/json?address=" + text + "&language=ru"
        geocodeURLString = geocodeURLString.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)!
        let _ = Alamofire.request(geocodeURLString, method: .get)
            .responseSwiftyJSON(completionHandler: { (request, response, json, error) in
                let results = json["results"]
                if results != "null" && json["status"] == "OK" {
                    var arr: [(String, CLLocationCoordinate2D)] = []
                    
                    for result in results {
                        let formatted_address = result.1["formatted_address"]
                        let lat = result.1["geometry"]["location"]["lat"]
                        let lng = result.1["geometry"]["location"]["lng"]
                        let coordinates = CLLocationCoordinate2D(latitude: CLLocationDegrees(String(describing: lat))!, longitude: CLLocationDegrees(String(describing: lng))!)
                        arr.append((String(describing: formatted_address), coordinates))
                    }
                    
                    result(arr)
                }
            })
    }
    
    func keyboardWillShow(notification: Notification) {
        if let userInfo = notification.userInfo {
            if let keyboardFrame = userInfo[UIKeyboardFrameBeginUserInfoKey] as? CGRect {
                let height: CGFloat = keyboardFrame.height
                let tableViewHeight: CGFloat = 110
                UIView.animate(withDuration: 0.3, animations: {
                    self.searchTableView.frame.size.height = tableViewHeight
                    self.searchTableView.frame.origin.y = self.view.frame.maxY - height - tableViewHeight
                    self.searchBar.frame.origin.y = self.searchTableView.frame.origin.y - 44
                })
            }
        }
    }
    
   
    
    func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.3, animations: {
            self.setStartedPositions()
        })
    }
    
    func addNotificationsForKeyboard() {
        let center = NotificationCenter.default
        center.addObserver(self,
                           selector: #selector(self.keyboardWillShow(notification:)),
                           name: .UIKeyboardWillShow,
                           object: nil)
        
        center.addObserver(self,
                           selector: #selector(self.keyboardWillHide(notification:)),
                           name: .UIKeyboardWillHide,
                           object: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        searchBar.resignFirstResponder()
    }
    

    func action(gestureRecognizer: UILongPressGestureRecognizer){
        let touchPoint = gestureRecognizer.location(in: mapView)
        let newCoordinates = mapView.convert(touchPoint, toCoordinateFrom: mapView)
        let annotation = MKPointAnnotation()
        annotation.coordinate = newCoordinates
        mapView.removeAnnotations(mapView.annotations)
        mapView.addAnnotation(annotation)
        
        let _ = Alamofire.request("https://maps.googleapis.com/maps/api/geocode/json?latlng=\(newCoordinates.latitude),\(newCoordinates.longitude)&language=ru", method: .get)
            .responseSwiftyJSON(completionHandler: { (request, response, json, error) in
                let results = json["results"]
                if results.exists() && json["status"].stringValue == "OK" {
                    let firstResult = results[0]
                    var city = ""
                    let address = firstResult["formatted_address"].stringValue
                    var country = ""
                    for component in firstResult["address_components"] {
                        let types = component.1["types"].arrayObject as! [String]
                        if types.contains("locality") {
                            city = component.1["long_name"].stringValue.lowercased()
                        }
                        
                        if types.contains("country") {
                            country = component.1["long_name"].stringValue.lowercased()
                        }
                        
                        if !city.isEmpty && !country.isEmpty {break}
                        
                    }
                    
                    selectedLocation = (address: address, coordinate: newCoordinates, country: country, city: city)
                    self.pop(sender: UIBarButtonItem())
                }
            })
      
    }
    
    func placemarkToNormalAddress(placemark: CLPlacemark) -> String {
        var exit = ""
        if placemark.thoroughfare != nil {
            exit += "\(placemark.thoroughfare!) "
        }
        if placemark.subThoroughfare != nil {
            exit += "\(placemark.subThoroughfare!) "
        }
        if placemark.locality != nil {
            exit += placemark.locality!
        }
        
        return exit
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setLeftBarButton()
    }
    
    func setLeftBarButton() {
        let barButton = UIBarButtonItem(image: #imageLiteral(resourceName: "iconBackArrow"), style: .plain, target: self, action: #selector(self.pop(sender:)))
        barButton.tintColor = .black
        navigationItem.leftBarButtonItem = barButton
    }
    
    func pop(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation { return nil }
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "") as? MKPinAnnotationView
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "")
            annotationView?.tintColor = UIColor.green  // do whatever customization you want
            annotationView?.canShowCallout = false            // but turn off callout
        } else {
            annotationView?.annotation = annotation
        }
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        // do something
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

