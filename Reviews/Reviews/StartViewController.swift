//
//  StartViewController.swift
//  Reviews
//
//  Created by Данил on 06.04.17.
//  Copyright © 2017 DevingLabs. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON

class StartViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {

    @IBOutlet weak var writeReviewButton: UIButton!
    @IBOutlet weak var searchReviewButton: UIButton!
    @IBOutlet weak var topTableView: UITableView!
    @IBOutlet weak var headerLabel: UILabel!
    
    let locationManager = CLLocationManager()
    
    var topCompanies: [Company] = [] {
        didSet {
            if topCompanies.count > 0 {
                topTableView.reloadData()
                
                UIView.animate(withDuration: 0.2) {
                    self.topTableView.alpha = self.topCompanies.count > 0 ? 1 : 0
                    self.headerLabel.alpha = self.topTableView.alpha
                }
            }
        }
    }
    
    var city = "" {
        didSet {
            Server().loadTopFiveCompaniesWithCity(city: city, completion: { (companies) in
                self.topCompanies = companies
            })
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if locations.count > 0  && city.isEmpty {
            
            let location = locations[0]
            let userCoords = location.coordinate
            
            let _ = Alamofire.request("https://maps.googleapis.com/maps/api/geocode/json?latlng=\(userCoords.latitude),\(userCoords.longitude)&language=ru", method: .get)
                .responseSwiftyJSON(completionHandler: { (request, response, json, error) in
                    let results = json["results"]

                    if results.exists() && json["status"].stringValue == "OK" {
                        let firstResult = results[0]
                        for component in firstResult["address_components"] {
                            let types = component.1["types"].arrayObject as! [String]
                            
                            if types.contains("locality") || types.contains("administrative_area_level_1") {
                                let city = component.1["long_name"].stringValue.lowercased().addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
                                if let city = city, self.city.isEmpty {
                                    self.city = city
                                }
                                break
                            }
                        }
                    }
                })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // load local
            topCompanies = LCFunc().loadTop5Locally()
        // then
        // reload top5 {
            city = ""
        // }
        
        navigationController?.hide()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        edgesForExtendedLayout = []
        
        //simple delegating and datasourcing
        topTableView.delegate = self
        topTableView.dataSource = self
        
        //launch location manager
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if topCompanies.count > 0 || LCFunc().loadTop5Locally().count > 0 {
            let company = topCompanies.count > 0 ? topCompanies[indexPath.row] : LCFunc().loadTop5Locally()[indexPath.row]
            let vc = storyboard?.instantiateViewController(withIdentifier: "companyVC") as! CompanyViewController
            vc.company = company
            vc.lastViewController = self
            self.navigationController!.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66   
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "topCompanyCell", for: indexPath) as! TopFiveTableViewCell
        let company = topCompanies[indexPath.row]
        cell.configure(company: company, place: indexPath.row + 1)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return topCompanies.count
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        navigationController?.hide()
        
        writeReviewButton.imageView?.contentMode = .scaleAspectFit
        searchReviewButton.imageView?.contentMode = .scaleAspectFit
    }

    func pop(sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    func setLeftBarButton() {
        let barButton = UIBarButtonItem(image: #imageLiteral(resourceName: "iconBackArrow"), style: .plain, target: self, action: #selector(self.pop(sender:)))
        barButton.tintColor = .black
        navigationItem.leftBarButtonItem = barButton
    }
    
    func setContentModeButtons() {
        writeReviewButton.imageView?.contentMode = .scaleAspectFit
        searchReviewButton.imageView?.contentMode = .scaleAspectFit
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
