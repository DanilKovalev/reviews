//
//  CountryTableViewCell.swift
//  Reviews
//
//  Created by Данил on 11.04.17.
//  Copyright © 2017 DevingLabs. All rights reserved.
//

import UIKit

class CountryTableViewCell: UITableViewCell {
    
    var countryLabel: UILabel!
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        initCountryLabel()
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureCellI(text: String, textUppercased: Bool) {
        guard let _ = countryLabel else {
            return
        }
        
        countryLabel.text = textUppercased ? text.capitalized : text
        
    }
    
    func initCountryLabel() {
        countryLabel = UILabel()
        countryLabel.translatesAutoresizingMaskIntoConstraints = false
        countryLabel.font = UIFont(name: "Helvetica", size: 16.0)
        countryLabel.numberOfLines = 3
        countryLabel.textColor = .black
        
        addSubview(countryLabel)
        
        let top = NSLayoutConstraint(item: countryLabel, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: countryLabel, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: countryLabel, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0)
        let leading = NSLayoutConstraint(item: countryLabel, attribute: .leadingMargin, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 20)
        
        addConstraints([top, bottom, trailing, leading])
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
