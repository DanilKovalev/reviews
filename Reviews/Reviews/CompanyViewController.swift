//
//  CompanyViewController.swift
//  Reviews
//
//  Created by Данил on 06.05.17.
//  Copyright © 2017 DevingLabs. All rights reserved.
//

import UIKit

enum SortType {
    case newIncrease
    case newDecrease
}

class CompanyViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var companyTableView: UITableView!
    
    var company: Company?
    
    var sortType: SortType = SortType.newDecrease {
        didSet {
            sortBy(type: sortType)
        }
    }
    
    var lastViewController: UIViewController!
    
    var reviews:[CompanyReview] = []
    
    func sortBy(type: SortType) {
        reviews = reviews.sorted(by: { (review1, review2) -> Bool in
            switch type {
                case .newDecrease: return review1.unix > review2.unix
                case .newIncrease: return review1.unix < review2.unix
            }
        })
        
        companyTableView.reloadData()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        companyTableView.delegate = self
        companyTableView.dataSource = self
        
        if let reviews = company!.reviews {
            self.reviews = reviews
            companyTableView.reloadData()
        }
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        navigationController?.show()
        calibriForNavigationBar()
        setLeftBarButton()
        setTableViewContentInset()
    }
    
    func setTableViewContentInset() {
        let contentInsetHeight = topLayoutGuide.length
        let contentInset = UIEdgeInsetsMake(contentInsetHeight, 0, 0, 0)
        
        companyTableView.contentInset = contentInset
        companyTableView.scrollIndicatorInsets = contentInset
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "routeOnMapSegue" {
            guard let vc = segue.destination as? MapViewController else {
                return
            }
            
            guard let company = self.company else {
                return
            }
            
            vc.route(coordinates: company.coordinates, completion: { route in
                vc.enterNavigatiorMode(withCompany: company, andRoute: route)
            })
        }
    }
    
    func showActionSheetController() {
        let optionMenu = UIAlertController(title: "Сортировка", message: "Выберите метод сортировки", preferredStyle: .actionSheet)
        
        let deleteAction = UIAlertAction(title: "Сначала новые", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.sortType = .newDecrease
        })
        
        let saveAction = UIAlertAction(title: "Сначала старые", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.sortType = .newIncrease
        })
        
        let cancelAction = UIAlertAction(title: "Отменить", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    
    func pop(sender: UIBarButtonItem) {
        
        if navigationController!.childViewControllers[navigationController!.childViewControllers.count - 2] is MapViewController {
            
            print("ROFLAN")
            
           (navigationController!.childViewControllers[navigationController!.childViewControllers.count - 2] as! MapViewController).isNeedToChangeTopLayoutGuide = false
        }
        
        smartBack()
    }
    
    func setLeftBarButton() {
        let barButton = UIBarButtonItem(image: #imageLiteral(resourceName: "iconBackArrow"), style: .plain, target: self, action: #selector(self.pop(sender:)))
        barButton.tintColor = .black
        navigationItem.leftBarButtonItem = barButton
    }
    
    func tappedAtSortView(sender: UILongPressGestureRecognizer) {
        let cell = companyTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! MainCompanyTableViewCell
        
        switch(sender.state) {
            case .began, .changed:
                cell.sortByButton.backgroundColor = .darkGray
            
            case UIGestureRecognizerState.ended:
                cell.sortByButton.backgroundColor = UIColor(netHex: 0xf1f1f1)
                showActionSheetController()
                
            default: break
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "mainCompanyCell", for: indexPath) as! MainCompanyTableViewCell
            
            let tapGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.tappedAtSortView(sender:)))
            tapGestureRecognizer.minimumPressDuration = 0
            cell.sortByButton.addGestureRecognizer(tapGestureRecognizer)
            
            //set data
            if let company = company {
                cell.configure(company: company)
            }
            
            cell.sortImage.image = sortType == .newDecrease ? #imageLiteral(resourceName: "textfieldDiscreese-closed") : #imageLiteral(resourceName: "textfieldDiscreese")
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "reviewCell", for: indexPath) as! ReviewTableViewCell
            
            let review = reviews[indexPath.row - 1]
            cell.configure(review: review)
            return cell
        }
    }
    

    @IBAction func showMoreButton(_ sender: UIButton) {
        let point = companyTableView.convert(CGPoint.zero, from: sender)
        
        guard let indexPath = companyTableView.indexPathForRow(at: point) else {
            return
        }
        
        let cell = companyTableView.cellForRow(at: indexPath) as! ReviewTableViewCell
        
        if let _ = cell.heightConstraint {
            cell.heightConstraint.isActive = false
            cell.layoutIfNeeded()
            cell.updateConstraints()
            companyTableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 300
        } else {
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 300
        } else {
            return 250
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviews.count + 1
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
