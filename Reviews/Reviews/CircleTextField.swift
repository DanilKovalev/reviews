//
//  CircleTextField.swift
//  Reviews
//
//  Created by Данил on 10.04.17.
//  Copyright © 2017 DevingLabs. All rights reserved.
//

import UIKit

public extension UIView {
    func embedStandartShadow() {
        layer.borderColor = UIColor.black.withAlphaComponent(0.14).cgColor
        layer.borderWidth = 0.7
        layer.masksToBounds = false
        layer.shadowRadius = 1.8
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOffset = CGSize(width: 1.2, height: 1.2)
        layer.shadowOpacity = 0.68
    }
    
    func embedMediumShadow() {
        layer.borderColor = UIColor.black.withAlphaComponent(0.14).cgColor
        layer.borderWidth = 0.7
        layer.masksToBounds = false
        layer.shadowRadius = 1.5
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOffset = CGSize(width: 1.5, height: 1.5)
        layer.shadowOpacity = 0.78

    }
}

class CircleTextField: UITextField {
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 30))
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0))
    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 30))
    }
    
    
    func on() {
        UIView.animate(withDuration: 0.25, animations: {
            self.setRightImage(image: #imageLiteral(resourceName: "cancel-music"))
        })
    }
    
    func off() {
        UIView.animate(withDuration: 0.25, animations: {
            self.setRightImage(image: self.text!.isEmpty ? #imageLiteral(resourceName: "textfieldDiscreese") : #imageLiteral(resourceName: "success-field"))
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        backgroundColor = .white
        
        font = UIFont(name: "Helvetica", size: 16.0)
        returnKeyType = .done
        embedStandartShadow()
        
        delay(delay: 0.3, closure: {
            self.setRightImage(image: #imageLiteral(resourceName: "textfieldDiscreese"))
        })
    }

    func setRightImage(image: UIImage) {
        for subview in subviews where subview.tag == 777 { subview.removeFromSuperview() }
        let imageView = UIImageView(frame: CGRect(x: bounds.size.width - frame.height * 0.44 - 15, y: 0, width: frame.height * 0.44, height: frame.height * 0.44))
        imageView.center.y = frame.height / 2
        imageView.contentMode = .scaleAspectFit
        imageView.image = image
        imageView.tag = 777
        addSubview(imageView)
        
        if image == #imageLiteral(resourceName: "cancel-music") {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tappedAtCross(sender:)))
            imageView.isUserInteractionEnabled = true
            imageView.addGestureRecognizer(tapGesture)
        }
        
        imageView.alpha = 0
        UIView.animate(withDuration: 0.3, animations: {
            imageView.alpha = 1
        })
    }
    
    func tappedAtCross(sender: UITapGestureRecognizer) {
        self.text = ""
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        cornerRadius = frame.size.height / 2
    }
    
   
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
