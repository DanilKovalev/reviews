//
//  Review.swift
//  Reviews
//
//  Created by Данил on 24.04.17.
//  Copyright © 2017 DevingLabs. All rights reserved.
//

import UIKit
import MapKit

class Review {
    
    /* ALL PROPERTIES IN REVIEW MUST BE LOWERCASSED */
    
    var sender: String?
    var text: String!
    var country: String!
    var city: String!
    var companyName: String!
    var companyID: String?
    var address: String!
    var isRecommend: Bool!
    var category: String!
    var isCompanySetted: Bool! = false
    var coordinate: CLLocationCoordinate2D!
    
    init(sender: String?, text: String!, country: String!, city: String!, companyName: String!, companyID: String!, address: String!, category: String, isRecommend: Bool!, isCompanySetted: Bool!, coordinate: CLLocationCoordinate2D!) {
        self.sender = sender
        self.coordinate = coordinate
        self.category = category
        self.text = text
        self.country = country
        self.city = city
        self.companyName = companyName
        self.companyID = companyID
        self.address = address
        self.isRecommend = isRecommend
        self.isCompanySetted = isCompanySetted
    }
    
    init(){}
}
