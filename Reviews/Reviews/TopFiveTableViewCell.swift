//
//  TopFiveTableViewCell.swift
//  Reviews
//
//  Created by Hadevs on 25.06.17.
//  Copyright © 2017 DevingLabs. All rights reserved.
//

import UIKit

class TopFiveTableViewCell: UITableViewCell {

    @IBOutlet weak var placeLabel: UILabel!
    @IBOutlet weak var logoView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var likeLabel: UILabel!
    @IBOutlet weak var dislikeLabel: UILabel!
    
    func configure(company: Company, place: Int) {
        
        //set photo
        if !company.logo.isEmpty {
            Server().loadPhotoOfCompany(company: company, completion: { (image) in
                self.logoView.image = image
            })
        }
        
        //set other info
        placeLabel.text = "\(place)"
        nameLabel.text = company.name.capitalized
        categoryLabel.text = company.category.capitalized
        likeLabel.text = "\(company.recommends)"
        dislikeLabel.text = "\(company.not_recommends)"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        logoView.layer.cornerRadius = logoView.frame.height / 2
        logoView.layer.masksToBounds = true
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
