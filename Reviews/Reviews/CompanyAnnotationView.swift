//
//  CompanyAnnotationView.swift
//  Reviews
//
//  Created by Данил on 05.05.17.
//  Copyright © 2017 DevingLabs. All rights reserved.
//

import UIKit


class CompanyAnnotationView: UIView {
    
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var likeImage: UIImageView!
    @IBOutlet weak var dislikeImage: UIImageView!
    @IBOutlet weak var likeLabel: UILabel!
    @IBOutlet weak var dislikeLabel: UILabel!
    @IBOutlet weak var categoryField: UILabel!
    @IBOutlet weak var buttonBackground: UIView!
    @IBOutlet weak var routeButton: UIButton!
    
    @IBAction func flff(_ sender: Any) {
        print("HOHOHOH")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
        logo.layer.cornerRadius = logo.frame.width / 2
        logo.layer.masksToBounds = true
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
