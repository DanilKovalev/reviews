//
//  NavigatorBottomView.swift
//  Reviews
//
//  Created by Hadevs on 08.07.17.
//  Copyright © 2017 DevingLabs. All rights reserved.
//

import UIKit

class NavigatorBottomView: UIView {

    @IBOutlet weak var endButton: UIButton!
    @IBOutlet weak var myLocationButton: UIButton!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        endButton.layer.cornerRadius = 5
        myLocationButton.layer.cornerRadius = myLocationButton.frame.height / 2
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
