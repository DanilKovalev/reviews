//
//  LocalFunctions.swift
//  Reviews
//
//  Created by Данил on 26.04.17.
//  Copyright © 2017 DevingLabs. All rights reserved.
//

import UIKit
import MapKit
import Alamofire
import SwiftyJSON

struct CompanyReview {
    var subId: String
    var id: String
    var text: String
    var sender: String
    var isRecommend: Bool
    var unix: Int
}

struct Company {
    var address: String
    var color: UIColor
    var logo: String
    var category: String
    var city: String
    var id: String
    var name: String
    var country: String
    var recommends: Int
    var not_recommends: Int
    var reviews: [CompanyReview]?
    var coordinates: CLLocationCoordinate2D
}

public var top5CompaniesJSON:[[String : Any]] = []

class LCFunc: NSObject {
    
    let defaults = UserDefaults.standard
    
    func saveTop5Locally() {
        defaults.set(top5CompaniesJSON, forKey: "topFive")
        defaults.synchronize()
    }

    func loadTop5Locally() -> [Company] {
        let json = JSON(defaults.value(forKey: "topFive") ?? [:])
        
        var companies: [Company] = []
        
        for object in json {
            let dict = object.1
            let id = dict["id"].stringValue
            let address = dict["address"].stringValue
            let category = dict["category"].stringValue
            let city = dict["city"].stringValue
            let name = dict["companyName"].stringValue
            let country = dict["country"].stringValue
            let likes = dict["recommends"]["likes"].intValue
            let dislikes = dict["recommends"]["dislikes"].intValue
            
            let stringLongitude = dict["longitude"].stringValue
            let stringLatitude = dict["latitude"].stringValue
            let longitude = CLLocationDegrees(stringLongitude)
            let latitude = CLLocationDegrees(stringLatitude)
            let coordinates = CLLocationCoordinate2D(latitude: latitude!, longitude: longitude!)
            
            let reviewsObject = dict["reviews"]
            
            var reviews:[CompanyReview] = []
            
            for reviewObject in reviewsObject {
                let id = reviewObject.0
                let subID = String(describing: reviewObject.1["id"])
                let text = String(describing: reviewObject.1["text"])
                let sender = String(describing: reviewObject.1["sender"])
                let isRecommend = Bool(String(describing: reviewObject.1["isRecommend"]))
                let unix = Int(String(describing: reviewObject.1["time"])) ?? 0
                let review = CompanyReview(subId: subID, id: id, text: text, sender: sender, isRecommend: isRecommend!, unix: unix)
                reviews.append(review)
            }
            let logo = String(describing: dict["logo"])
            companies.append(Company(address: address, color: UIColor().random(), logo: logo, category: category, city: city, id: id, name: name, country: country, recommends: likes, not_recommends: dislikes, reviews: reviews, coordinates: coordinates))
        }

        return companies
    }
    
    func isReviewValid(review: Review) -> Bool {
        guard let _ = review.address else {
            return false
        }
        
        guard let _ = review.category else {
            return false
        }
        
        guard let _  = review.city else {
            return false
        }
        
        guard let _  = review.companyName else {
            return false
        }
        
        guard let _ = review.country else {
            return false
        }
        
        guard let _ = review.isRecommend else {
            return false
        }
        
        guard let _  = review.text else {
            return false
        }
        
        return true
    }
    
    func searchByStreet(text: String, completion: @escaping (_ results: [String]) -> Void) {
        var results: [String] = []
        
        DispatchQueue.main.async {
            
            CLGeocoder().geocodeAddressString(text) { (placemarks, error) in
                if placemarks != nil && placemarks!.count > 0 {
                    for placemark in placemarks! {
                        if placemark.addressDictionary?["FormattedAddressLines"] != nil {
                            results.append((placemark.addressDictionary!["FormattedAddressLines"]! as! [String]).joined(separator: ", "))
                        }
                    }
                    completion(results)
                }
            }
        }
    }
    
    func searchByCompany(name: String?, category: String?, city: String?, country: String?, completion: @escaping (_ companies: [Company]) -> Void) {
        let name = name ?? ""; let category = category ?? ""; let city = city ?? ""; let country = country ?? "";
        
        DispatchQueue.main.async {
            Server().postJSONArray(url: serverIP + "/searchCompany", parameters: [
                "name": name,
                "category": category,
                "city": city,
                "country": country,
                ], completion: { (json) in
                   
                    if json != nil {
                        var companies: [Company] = []
                        for object in json! {
                            let dict = object.1
                            let reviewsObject = dict["reviews"]
                            
                            var reviews:[CompanyReview] = []
                            
                            for reviewObject in reviewsObject {
                                let id = reviewObject.0
                                let subID = String(describing: reviewObject.1["id"])
                                let text = String(describing: reviewObject.1["text"])
                                let sender = String(describing: reviewObject.1["sender"])
                                let isRecommend = Bool(String(describing: reviewObject.1["isRecommend"]))
                                let unix = Int(String(describing: reviewObject.1["time"]))!
                                let review = CompanyReview(subId: subID, id: id, text: text, sender: sender, isRecommend: isRecommend!, unix: unix)
                                reviews.append(review)
                            }
                            
                            let lng = CLLocationDegrees(String(describing: object.1["longitude"]))!
                            let lat = CLLocationDegrees(String(describing: object.1["latitude"]))!
                            let coordinates = CLLocationCoordinate2D(latitude: lat, longitude: lng)

                            let company = Company(
                                address: String(describing: object.1["address"]), color: UIColor().random(), logo: String(describing: object.1["logo"]),
                                category: String(describing: object.1["category"]),
                                city: String(describing: object.1["city"]),
                                id: String(describing: object.1["_id"]),
                                name: String(describing: object.1["companyName"]),
                                country: String(describing: object.1["country"]),
                                recommends: Int(String(describing: object.1["recommends"]["likes"]))!,
                                not_recommends: Int(String(describing: object.1["recommends"]["dislikes"]))!, reviews: reviews, coordinates: coordinates)
                            companies.append(company)
                        }
                        completion(companies)
                    } else {
                        //json is nil
                        completion([])
                    }
            })
            
        }
    }
    
    func searchByCity(text: String) -> [String] {
        let filteredStrings = cities.filter({(item: String) -> Bool in
            let stringMatch = item.lowercased().range(of: text.lowercased())
            return stringMatch != nil ? true : false
        })
        return filteredStrings
    }
    
    func searchByCountry(text: String) -> [String] {
        let filteredStrings = countries.filter({(item: String) -> Bool in
            let stringMatch = item.lowercased().range(of: text.lowercased())
            return stringMatch != nil ? true : false
        })
        return filteredStrings
    }
}
