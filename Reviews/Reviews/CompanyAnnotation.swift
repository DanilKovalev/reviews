//
//  CompanyAnnotationView.swift
//  Reviews
//
//  Created by Данил on 05.05.17.
//  Copyright © 2017 DevingLabs. All rights reserved.
//

import UIKit
import MapKit

class CompanyAnnotation: NSObject, MKAnnotation {
    
    var coordinate: CLLocationCoordinate2D
    var company: Company
    
    init(coordinate: CLLocationCoordinate2D, company: Company) {
        self.coordinate = coordinate
        self.company = company
    }
}
