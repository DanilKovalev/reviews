//
//  Server.swift
//  Reviews
//
//  Created by Данил on 03.05.17.
//  Copyright © 2017 DevingLabs. All rights reserved.
//

import UIKit
import Alamofire
import MapKit
import SwiftyJSON

class Server: NSObject {
    
    let defaults = UserDefaults.standard
    
    func loadTopFiveCompaniesWithCity(city: String, completion: @escaping (_ companies: [Company]) -> Void) {
        let _ = Alamofire.request("http://89.223.92.150:27018/getTopFive/\(city)", method: .get)
        .responseSwiftyJSON { (request, response, json, error) in
            if json.exists() {
                
                if json.count > 0 {
                    
                    top5CompaniesJSON = (json.arrayObject as? [[String: Any]]) ?? []
                    LCFunc().saveTop5Locally()
                }
                
                var companies: [Company] = []
                
                for object in json {
                    let dict = object.1
                    let id = dict["id"].stringValue
                    let address = dict["address"].stringValue
                    let category = dict["category"].stringValue
                    let city = dict["city"].stringValue
                    let name = dict["companyName"].stringValue
                    let country = dict["country"].stringValue
                    let likes = dict["recommends"]["likes"].intValue
                    let dislikes = dict["recommends"]["dislikes"].intValue
                    
                    let stringLongitude = dict["longitude"].stringValue
                    let stringLatitude = dict["latitude"].stringValue
                    let longitude = CLLocationDegrees(stringLongitude)
                    let latitude = CLLocationDegrees(stringLatitude)
                    let coordinates = CLLocationCoordinate2D(latitude: latitude!, longitude: longitude!)
                    
                    let reviewsObject = dict["reviews"]
                    
                    var reviews:[CompanyReview] = []
                    
                    for reviewObject in reviewsObject {
                        let id = reviewObject.0
                        let subID = String(describing: reviewObject.1["id"])
                        let text = String(describing: reviewObject.1["text"])
                        let sender = String(describing: reviewObject.1["sender"])
                        let isRecommend = Bool(String(describing: reviewObject.1["isRecommend"]))
                        let unix = Int(String(describing: reviewObject.1["time"])) ?? 0
                        let review = CompanyReview(subId: subID, id: id, text: text, sender: sender, isRecommend: isRecommend!, unix: unix)
                        reviews.append(review)
                    }
                    let logo = String(describing: dict["logo"])
                    companies.append(Company(address: address, color: UIColor().random(), logo: logo, category: category, city: city, id: id, name: name, country: country, recommends: likes, not_recommends: dislikes, reviews: reviews, coordinates: coordinates))
                }
                
                completion(companies)
                return
            } else {
                completion([])
                return
            }
        }
    }
    
    func loadPhotoOfCompany(company: Company, completion: @escaping (_ photo: UIImage) -> Void) {
        DispatchQueue.main.async {
            let url = URL(string: company.logo)
            do {
                let data = try Data(contentsOf: url!)
                completion(UIImage(data: data)!)
            } catch {}
        }
    }
    
    func loadAllCompanies(completion: @escaping (_ companies: [Company]?) -> Void) {
        @discardableResult let _ = Alamofire.request(serverIP + "/companies", method: .get)
            .responseSwiftyJSON { (request, response, json, error) in
                if error != nil {
                    print("Error in /Companies request: \(error.debugDescription)")
                    completion(nil)
                } else {
                    var companies: [Company] = []
                    
                    for object in json {
                        let dict = object.1
                        let id = String(describing: dict["id"])
                        let address = String(describing: dict["address"])
                        let category = String(describing: dict["category"])
                        let city = String(describing: dict["city"])
                        let name = String(describing: dict["companyName"])
                        let country = String(describing: dict["country"])
                        let likes = Int(String(describing: dict["recommends"]["likes"]))!
                        let dislikes = Int(String(describing: dict["recommends"]["dislikes"]))!
                        
                        let stringLongitude = String(describing: dict["longitude"])
                        let stringLatitude = String(describing: dict["latitude"])
                        let longitude = CLLocationDegrees(stringLongitude)
                        let latitude = CLLocationDegrees(stringLatitude)
                        let coordinates = CLLocationCoordinate2D(latitude: latitude!, longitude: longitude!)
                        
                        let reviewsObject = dict["reviews"]
                        
                        var reviews:[CompanyReview] = []
                        
                        for reviewObject in reviewsObject {
                            let id = reviewObject.0
                            let subID = String(describing: reviewObject.1["id"])
                            let text = String(describing: reviewObject.1["text"])
                            let sender = String(describing: reviewObject.1["sender"])
                            let isRecommend = Bool(String(describing: reviewObject.1["isRecommend"]))
                            let unix = Int(String(describing: reviewObject.1["time"])) ?? 0
                            let review = CompanyReview(subId: subID, id: id, text: text, sender: sender, isRecommend: isRecommend!, unix: unix)
                            reviews.append(review)
                        }
                        let logo = String(describing: dict["logo"])
                        companies.append(Company(address: address, color: UIColor().random(), logo: logo, category: category, city: city, id: id, name: name, country: country, recommends: likes, not_recommends: dislikes, reviews: reviews, coordinates: coordinates))
                    }
                    completion(companies)
                }
        }
    }
    
    func createCompanyBy(review: Review, completion: @escaping (_ id: String?) -> Void) {
        
        postJSONArray(url: serverIP + "/newCompany", parameters: [
            "longitude": String(describing: review.coordinate.longitude),
            "latitude": String(describing: review.coordinate.latitude),
            "address": review.address!,
            "category": review.category!,
            "city": review.city!,
            "companyID": review.companyID!,
            "companyName": review.companyName!,
            "country": review.country!
            ]) { (json) in
                if json != nil {
                    let id = String(describing: json!["ops"][0]["_id"])
                    completion(id)
                } else {
                    completion(nil)
                }
        }
    }
    
    func createReview(review: Review, completion: @escaping (_ success: Bool) -> Void) {
        postJSONArray(url: serverIP + "/newReview", parameters: [
            "companyID": review.companyID!,
            "text": review.text!,
            "sender": review.sender ?? "Аноним",
            "isRecommend": review.isRecommend!,
            "time": Int(Date().timeIntervalSince1970)
            ]) { (json) in
                let unix = self.defaults.integer(forKey: review.companyID!)
                let lastTimeCreated = Date(timeIntervalSince1970: TimeInterval(unix))
                let countOfNumbers = lastTimeCreated.months(from: Date())
                
                print(unix, lastTimeCreated, countOfNumbers)
                
                self.defaults.set(Date().timeIntervalSince1970, forKey: review.companyID!)
                self.defaults.synchronize()
                
                completion(countOfNumbers > 1 || countOfNumbers < 0)
        }
    }
    
    func postJSONArray(url: String, parameters: [String: Any], completion: @escaping (_ json: JSON?) -> Void) {
        let request = NSMutableURLRequest(url: URL(string: url)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let values = parameters
        request.httpBody = try! JSONSerialization.data(withJSONObject: values, options: [])
        
        Alamofire.request(request as URLRequest)
            .responseJSON { response in
                // do whatever you want here
                switch response.result {
                case .failure(_):
                    completion(nil)
                case .success(let responseObject):
                    let json = JSON(responseObject)
                    completion(json)
        }
    }
}
}
