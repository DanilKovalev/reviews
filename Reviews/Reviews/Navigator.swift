//
//  Navigator.swift
//  Reviews
//
//  Created by Hadevs on 08.07.17.
//  Copyright © 2017 DevingLabs. All rights reserved.
//

import MapKit

class Navigator: NSObject {
    var endPoint: CLLocationCoordinate2D
    var startPoint: CLLocationCoordinate2D
    var transportType: MKDirectionsTransportType = .automobile
    
    init(endPoint: CLLocationCoordinate2D, startPoint: CLLocationCoordinate2D, transportType: MKDirectionsTransportType) {
        self.endPoint = endPoint
        self.startPoint = startPoint
        self.transportType = transportType
        
        super.init()
    }
    
    init(endPoint: CLLocationCoordinate2D, startPoint: CLLocationCoordinate2D) {
        self.endPoint = endPoint
        self.startPoint = startPoint
        
        super.init()
    }
    
    func route(completion: @escaping (_ route: MKRoute?) -> Void) {
        let startPlacemark = MKPlacemark(coordinate: startPoint, addressDictionary: nil)
        let endPlacemark = MKPlacemark(coordinate: endPoint, addressDictionary: nil)
        
        let startMapItem = MKMapItem(placemark: startPlacemark)
        let endMapItem = MKMapItem(placemark: endPlacemark)
        
        let directionRequest = MKDirectionsRequest()
        directionRequest.source = startMapItem
        directionRequest.destination = endMapItem
        directionRequest.transportType = transportType
        
        let directions = MKDirections(request: directionRequest)
        directions.calculate { (response, error) in
            guard let response = response, response.routes.count > 0 else {
                NSLog("Calculate directions failed with error: \(error!.localizedDescription)")
                completion(nil)
                return
            }
            
            let route = response.routes[0]
        
            completion(route)
        }
    }
}
