//
//  Darkness.swift
//  Reviews
//
//  Created by Данил on 11.04.17.
//  Copyright © 2017 DevingLabs. All rights reserved.
//

import UIKit

class Darkness: UIView {
    var main_alpha: CGFloat = 0
    init(alpha: CGFloat) {
        super.init(frame: UIScreen.main.bounds)
        backgroundColor = .black
        main_alpha = alpha
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func show() {
        UIView.animate(withDuration: 0.25, animations: {
            self.alpha = self.main_alpha
        })
    }
    
    func hide() {
        UIView.animate(withDuration: 0.25, animations: {
            self.alpha = 0
        }, completion: { (true) in
            self.removeFromSuperview()
        })
    }
}
