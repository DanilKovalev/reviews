//
//  ScaleButton.swift
//  Reviews
//
//  Created by Данил on 17.04.17.
//  Copyright © 2017 DevingLabs. All rights reserved.
//

import UIKit

class ScaleButton: UIButton {

    func scaleOut() {
        UIView.animate(withDuration: 0.1, animations: {
            self.transform = CGAffineTransform(scaleX: 1.18,y: 1.18)
        }, completion:{ (true) in
            UIView.animate(withDuration: 0.11, animations: {
                self.transform = CGAffineTransform.identity
            })
        })
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        adjustsImageWhenHighlighted = false
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView?.contentMode = .scaleAspectFit
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.next?.touchesBegan(touches, with: event)
    }
}
