//
//  NavigatorTopView.swift
//  Reviews
//
//  Created by Hadevs on 08.07.17.
//  Copyright © 2017 DevingLabs. All rights reserved.
//

import UIKit
import MapKit

class NavigatorTopView: UIView {

    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var unitDistanceLabel: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!
    
    func configure(company: Company, route: MKRoute) {
        let distance = route.distance // in meters
        
        if distance >= 1000 {
            distanceLabel.text = "\(Int(distance / 100) / 10)"
            unitDistanceLabel.text = "км"
        } else {
            distanceLabel.text = "\(Int(distance))"
            unitDistanceLabel.text = "м"
        }
        
        companyNameLabel.text = company.name.capitalized
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
