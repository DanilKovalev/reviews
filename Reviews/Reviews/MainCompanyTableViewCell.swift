//
//  MainCompanyTableViewCell.swift
//  Reviews
//
//  Created by Данил on 06.05.17.
//  Copyright © 2017 DevingLabs. All rights reserved.
//

import UIKit
import MapKit

class MainCompanyTableViewCell: UITableViewCell {

    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var likesCountLabel: UILabel!
    @IBOutlet weak var dislikesCountLabel: UILabel!
    @IBOutlet weak var backgroundStateView: UIView!
    @IBOutlet weak var sortByButton: UIView!
    @IBOutlet weak var sortLabel: UILabel!
    @IBOutlet weak var sortImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        // Initialization code
    }
    
    func configure(company: Company) {
        //load logo
        logoImageView.image = nil
        if !company.logo.isEmpty {
            Server().loadPhotoOfCompany(company: company, completion: { (image) in
                self.logoImageView.image = image
            })
        }
        
        //load other info
        companyNameLabel.text = company.name.capitalized
        addressLabel.text = company.address.capitalized
        likesCountLabel.text = "\(company.recommends)"
        dislikesCountLabel.text = "\(company.not_recommends)"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        backgroundStateView.layer.cornerRadius = 18
        backgroundStateView.layer.shadowColor = UIColor.black.cgColor
        backgroundStateView.layer.shadowOpacity = 0.2
        backgroundStateView.layer.shadowOffset = CGSize.zero
        backgroundStateView.layer.shadowRadius = 1.8
        sortByButton.layer.cornerRadius = sortByButton.frame.height / 2
        
        logoImageView.layer.cornerRadius = logoImageView.frame.height / 2
        logoImageView.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension UIColor {
    func random() -> UIColor {
        let colors = [0x28abe2, 0x8cc63f, 0xfbaf3b, 0xf05a23]
        return UIColor(netHex: colors[Int(arc4random_uniform(UInt32(colors.count)))])
    }
}

extension Float {
    func random() -> CGFloat {
        return CGFloat(arc4random_uniform(100)) / CGFloat(100.0)
    }
}
