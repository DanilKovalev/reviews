//
//  CompanyTableViewCell.swift
//  Reviews
//
//  Created by Данил on 06.05.17.
//  Copyright © 2017 DevingLabs. All rights reserved.
//

import UIKit

class CompanyTableViewCell: UITableViewCell {

    @IBOutlet weak var companyDescriptionLabel: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var dislikeCountLabel: UILabel!
    @IBOutlet weak var likesCountLabel: UILabel!
    @IBOutlet weak var logoView: UIImageView!
    @IBOutlet weak var addressLabel: UILabel!
    
    func configureCell(company: Company) {

        companyDescriptionLabel.text = company.category.capitalized
        companyNameLabel.text = company.name.capitalized
        dislikeCountLabel.text = "\(company.not_recommends)"
        likesCountLabel.text = "\(company.recommends)"
        addressLabel.text = "\(company.address)"
        
        logoView.image = nil
        if !company.logo.isEmpty {
            Server().loadPhotoOfCompany(company: company, completion: { (image) in
                self.logoView.image = image
            })
        }
        
//        companyNameLabel.sizeToFit()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        logoView.layer.cornerRadius = logoView.frame.width / 2
        logoView.layer.masksToBounds = true
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
