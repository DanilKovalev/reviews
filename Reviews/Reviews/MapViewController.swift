//
//  MapViewController.swift
//  Reviews
//
//  Created by Данил on 05.05.17.
//  Copyright © 2017 DevingLabs. All rights reserved.
//

import UIKit
import MapKit
import ARSLineProgress
import Alamofire
import SwiftyJSON

public var currentUserCoordinates = CLLocationCoordinate2D()

class TravelledPolyline: MKPolyline {}

class MapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var calloutView: CompanyAnnotationView!
    @IBOutlet weak var navigatorTopView: NavigatorTopView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    var isNeedToChangeTopLayoutGuide = false
    
    var lastSelectedCompany: Company?
    var navigator: Navigator!
    private var locationManager: CLLocationManager!
    
    func initLocationManager() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
    }
    
    
    private var lastSteppedCoordinate: CLLocationCoordinate2D?
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let newStep = locations.first, let firstStep = lastSteppedCoordinate else {
            return
        }
        
        let polyline = TravelledPolyline(coordinates: [newStep.coordinate, firstStep], count: 2)
        mapView.add(polyline, level: .aboveRoads)
    }
    
    var calloutHeight: CGFloat {
        return self.view.frame.width * 0.4368 - 10
    }
 
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = overlay is TravelledPolyline ? UIColor(netHex: 0xf1f1f1) : UIColor(netHex: 0x3678F5)
        renderer.lineWidth = 4.0
        
        return renderer
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if view.annotation is MKUserLocation { return }
    
        let companyAnnotation = view.annotation as! CompanyAnnotation
        exitNavigatorMode()
        selectCompany(companyAnnotation: companyAnnotation, view: view)
    }
    
    func route(coordinates: CLLocationCoordinate2D, completion: @escaping (_ route: MKRoute) -> Void) {

        ARSLineProgress.show()
        
        navigator = Navigator(endPoint: coordinates, startPoint: currentUserCoordinates)
        navigator.route {
            route in
            guard let route = route else {
                ARSLineProgress.showFail()
                self.showAlert(title: "Ошибка", message: "Сервис не может проложить маршрут к этой компании.")
                return
            }
            
            ARSLineProgress.hide()
            
            self.mapView.removeOverlays(self.mapView.overlays)
            
            self.mapView.add((route.polyline), level: MKOverlayLevel.aboveRoads)
            
            let rect = route.polyline.boundingMapRect
            
            self.mapView.setRegion(MKCoordinateRegionForMapRect(rect), animated: true)
            
            self.delay(delay: 1) {
                self.mapView.setZoomByDelta(delta: 1.3, animated: true)
            }
            
            self.delay(delay: 3, closure: {
                let userSpan = MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005)
                let userRegion = MKCoordinateRegion(center: currentUserCoordinates, span: userSpan)
                self.mapView.setRegion(userRegion, animated: true)
                
                self.delay(delay: 0.65, closure: {
                    self.mapView.userTrackingMode = .followWithHeading
                })
                
            })
            completion(route)
        }
    }
    
    func routeButtonSelected(sender: UIButton) {
        guard let company = lastSelectedCompany else {
            return
        }
        
        route(coordinates: company.coordinates, completion: { route in
            self.enterNavigatiorMode(withCompany: company, andRoute: route)
        })
    }
    
    func selectCompany(companyAnnotation: CompanyAnnotation, view: MKAnnotationView) {
        if let _ = calloutView {
            
            calloutView.name.text = companyAnnotation.company.name.capitalized
            calloutView.address.text = companyAnnotation.company.address
            calloutView.categoryField.text = companyAnnotation.company.category.capitalized
            
            calloutView.dislikeLabel.text = String(describing: companyAnnotation.company.not_recommends)
            calloutView.likeLabel.text = String(describing: companyAnnotation.company.recommends)
            calloutView.likeLabel.sizeToFit()
            calloutView.dislikeLabel.sizeToFit()
            
            calloutView.routeButton.addTarget(self, action: #selector(self.routeButtonSelected(sender:)), for: .touchUpInside)

            //========================
            lastSelectedCompany = companyAnnotation.company
            //========================
            
            calloutView.logo.image = nil
            if !companyAnnotation.company.logo.isEmpty {
                Server().loadPhotoOfCompany(company: companyAnnotation.company, completion: { (image) in
                    self.calloutView.logo.image = image
                })
            }
            
            let gesture = UILongPressGestureRecognizer(target: self, action: #selector(self.tappedAtCallout(sender:)))
            gesture.minimumPressDuration = 0
            calloutView.isUserInteractionEnabled = true
            calloutView.addGestureRecognizer(gesture)
            
            calloutView.layer.cornerRadius = 15
            calloutView.layer.masksToBounds = true
            calloutView.layer.borderColor = UIColor(netHex: 0x707070).cgColor
            calloutView.layer.borderWidth = 0.7
            
            calloutView.tag = 2
            
            UIView.animate(withDuration: 0.2, animations: {
                self.calloutView.alpha = 1
                self.calloutView.routeButton.superview?.alpha = 1
            })
            
            mapView.setCenter((view.annotation?.coordinate)!, animated: true)
        }
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
            if view.isKind(of: AnnotationView.self) {
                hideBottomView()
        }
    }
    
    func hideBottomView() {
        for subview in self.view.subviews where subview.tag == 2 { UIView.animate(withDuration: 0.2, animations: {
                subview.alpha = 0
                self.calloutView.routeButton.superview?.alpha = 0
            })
        }
    }
    
    func enterNavigatiorMode(withCompany company: Company, andRoute route: MKRoute) {
        
        
        hideBottomView()
        
        //remove all created views with this type
        for subview in view.subviews where subview is NavigatorBottomView { subview.removeFromSuperview() }
        
        // add top view
        navigatorTopView.configure(company: company, route: route)
        
        // add bottom view
        let bottomView = UINib(nibName: "NavigatorBottomView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! NavigatorBottomView
        bottomView.endButton.addTarget(self, action: #selector(self.endButtonNavigatorClicked(sender:)), for: .touchUpInside)
        bottomView.myLocationButton.addTarget(self, action: #selector(self.myLocationButtonClicked(sender:)), for: .touchUpInside)
        
        let bottomRect = CGRect(x: 0, y: self.view.frame.height - bottomView.frame.height - 5, width: view.frame.width, height: 70)
        bottomView.frame = bottomRect
        
        view.addSubview(bottomView)
        
        bottomView.alpha = 0
        navigatorTopView.alpha = 0

        //animate to normal position
        UIView.animate(withDuration: 0.3, animations: {
            bottomView.alpha = 1
            self.navigatorTopView.alpha = 1
        })
    }

    func myLocationButtonClicked(sender: UIButton) {
        mapView.setCenter(currentUserCoordinates, animated: true)
    }
    
    func endButtonNavigatorClicked(sender: UIButton) {
        let alertController = UIAlertController(title: "Подтверждение", message: "Вы уверены, что хотите завершить маршрут?", preferredStyle: .alert)
        
        let yesAction = UIAlertAction(title: "Да", style: .destructive) { (_) in
            self.exitNavigatorMode()
        }
        
        let noAction = UIAlertAction(title: "Нет", style: .default) { (_) in}
        
        alertController.addAction(yesAction)
        alertController.addAction(noAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func exitNavigatorMode() {
        for subview in view.subviews where subview is NavigatorBottomView {
            UIView.animate(withDuration: 0.4, animations: {
                subview.alpha = 0
                self.navigatorTopView.alpha = 0
            }, completion: { (true) in
                subview.removeFromSuperview()
            })
        }
        
        mapView.removeOverlays(mapView.overlays)
        mapView.userTrackingMode = .followWithHeading
    }
    
    func tappedAtCallout(sender: UILongPressGestureRecognizer) {
        switch sender.state {
            case .began:
                calloutView.layer.backgroundColor = UIColor.darkGray.cgColor
            case .ended:
                calloutView.layer.backgroundColor = UIColor.clear.cgColor
                if let company = lastSelectedCompany {
                    let vc = storyboard?.instantiateViewController(withIdentifier: "companyVC") as! CompanyViewController
                    vc.company = company
                    vc.lastViewController = self
                    navigationController?.pushViewController(vc, animated: true)
                }
            default: break
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard !(annotation is MKUserLocation) else {
            return nil
        }
        
        let annotationIdentifier = "companyPin"
        var annotationView: MKAnnotationView?
        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeuedAnnotationView
            annotationView?.annotation = annotation
        }
        else {
            annotationView = AnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        
        if let annotationView = annotationView {
            annotationView.canShowCallout = false
            annotationView.image = #imageLiteral(resourceName: "pin")
        }
        return annotationView
    }

    
    func loadCompanies() {
        ARSLineProgress.show()
        
        Server().loadAllCompanies { (companies) in
            if companies == nil {
                ARSLineProgress.showFail()
            } else {
                ARSLineProgress.showSuccess()
                self.showCompaniesOnMap(companies: companies!)
            }
        }
    }
    
    func showCompaniesOnMap(companies: [Company]) {
        for company in companies {
            
            let coordinate = company.coordinates
            let annotation = CompanyAnnotation(coordinate: coordinate, company: company)
            
            self.mapView.addAnnotation(annotation)
        }
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        navigatorTopView.alpha = 0
        
        title = "НАЙТИ ОТЗЫВ НА КАРТЕ"

        mapView.delegate = self
        mapView.userTrackingMode = .follow
        loadCompanies()
        mapView.subviews[1].isHidden = true
        
        calloutView.alpha = 0
        calloutView.routeButton.superview?.alpha = 0
        initLocationManager()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        navigationController?.show()
        setLeftBarButton()
        
        topConstraint.constant = isNeedToChangeTopLayoutGuide ? 42 : 0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        topConstraint.constant = isNeedToChangeTopLayoutGuide ? 42 : 0
    }
    
    func setLeftBarButton() {
        let barButton = UIBarButtonItem(image: #imageLiteral(resourceName: "iconBackArrow"), style: .plain, target: self, action: #selector(self.pop(sender:)))
        barButton.tintColor = .black
        navigationItem.leftBarButtonItem = barButton
    }
    
    func pop(sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MKMapView {
    
    func setZoomByDelta(delta: Double, animated: Bool) {
        var _region = region;
        var _span = region.span;
        _span.latitudeDelta *= delta;
        _span.longitudeDelta *= delta;
        _region.span = _span;
        
        setRegion(_region, animated: animated)
    }
}
