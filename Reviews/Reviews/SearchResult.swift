//
//  SearchResult.swift
//  Reviews
//
//  Created by Данил on 11.04.17.
//  Copyright © 2017 DevingLabs. All rights reserved.
//

import UIKit

public enum SearchType {
    case country
    case city
    case address
    case company
    case category
    case null
    //Types: Country, City, Address - {Strings}; Company - {Company}, Category - ???
}

class SearchResults {
    
    var type: SearchType = .null
    var data: [Any] = []
    
    func clear() {
        data = []
        type = .null
    }
    
}
