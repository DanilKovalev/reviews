//
//  WriteReviewTableViewController.swift
//  Reviews
//
//  Created by Данил on 12.04.17.
//  Copyright © 2017 DevingLabs. All rights reserved.
//

import UIKit
import Fakery
import MapKit
import ARSLineProgress

class WriteReviewViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var nameField: CircleTextField!
    @IBOutlet weak var addressField: CircleTextField!
    @IBOutlet weak var categoryField: CircleTextField!
    @IBOutlet weak var companyNameField: CircleTextField!
    @IBOutlet weak var reviewView: UIView!
    @IBOutlet weak var reviewTextView: UITextView!
    @IBOutlet weak var recommendButton: UIButton!
    @IBOutlet weak var notRecommendButton: UIButton!
    @IBOutlet weak var sendReviewButton: UIButton!
    
    // static vars
    var searchResults = SearchResults()
    var keyboardRect = CGRect()
    var review = Review()
    var fakery = Faker()
    
    // static lets
    private let REVIEW_PLACEHOLDER_TEXT = "Напишите Ваш отзыв"
    private let REVIEW_PLACEHOLDER_COLOR = UIColor(netHex: 0xBFBFC4)
    private let REVIEW_DISABLE_COLOR = UIColor(netHex: 0xf1f1f1)
    
    let locationManager = CLLocationManager()
    
    // UI vars
    var activeTextField = UITextField()
    var searchResultsTableView: UITableView!
    var dark: Darkness!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        edgesForExtendedLayout = []
        
        selectedLocation = (address: "", coordinate: CLLocationCoordinate2D(), country: "", city: "")
        
        initDark()
        initLocationManager()
        initTableView()
        
        reviewView.clipsToBounds = true
        reviewView.layer.cornerRadius = 18
        
        configureReviewTextViewPlaceholder()
        delegating()
        addNotificationsForKeyboard()
        
        reviewTextView.textContainerInset = UIEdgeInsetsMake(10, 10, 0, 0)
//        fillFakeData() // optional
    }
    
    func initLocationManager() {
        locationManager.delegate = self
        
        if CLLocationManager.authorizationStatus() == .notDetermined {
            locationManager.requestAlwaysAuthorization()
        }
        else if CLLocationManager.authorizationStatus() == .denied {
            showAlert(title: "Error", message: "Location services were previously denied. Please enable location services for this app in Settings.")
        }
        else if CLLocationManager.authorizationStatus() == .authorizedAlways {
            locationManager.startUpdatingLocation()
        }
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }

    
    func clearAll() {
        review = Review()
        for subview in view.subviews where subview is CircleTextField {
            (subview as! CircleTextField).text = ""
            (subview as! CircleTextField).setRightImage(image: #imageLiteral(resourceName: "textfieldDiscreese"))
            (subview as! CircleTextField).isEnabled = true
        }

        reviewTextView.text = ""
    }
    
    func fillFakeData() {
        let name = fakery.app.name()
        let address = fakery.address.streetAddress()
        let category = fakery.lorem.word()
        let companyName = fakery.company.name()
//        let companyName = "te"
        let city = fakery.address.city()
        let country = fakery.address.county()
        let text = fakery.lorem.sentence()
        review = Review(sender: name, text: text, country: country, city: city, companyName: companyName, companyID: ID(), address: address, category: category, isRecommend: true, isCompanySetted: false, coordinate: CLLocationCoordinate2D())
        
        nameField.text = name
        addressField.text = address
        categoryField.text = category
        companyNameField.text = companyName
        reviewTextView.text = text
    }
    
    @IBAction func sendButtonClicked(sender: UIButton) {
            ARSLineProgress.show()
            if LCFunc().isReviewValid(review: self.review) {
                self.review.companyID = self.review.isCompanySetted! ? self.review.companyID! : self.ID()
                self.review.category = categoryField.text ?? categories.first!
                if !self.review.isCompanySetted {
                    Server().createCompanyBy(review: self.review, completion: { (id) in
                        if id != nil {
                            self.review.companyID = id!
                            Server().createReview(review: self.review, completion: { (success) in
                                // не нужно проверки, потому что это новая компания
                                    ARSLineProgress.showSuccess()
                                    self.clearAll()
                                    self.adjustViewControllerToNewReview()
                            })
                        } else {
                            ARSLineProgress.showFail()
                        }
                    })
                } else {
                    Server().createReview(review: self.review, completion: { (success) in
                        if success {
                            ARSLineProgress.showSuccess()
                            self.clearAll()
                            self.adjustViewControllerToNewReview()
                        } else {
                            ARSLineProgress.showFail()
                            self.showAlert(title: "Ошибка", message: "Вы уже оставляли отзыв на эту компанию в этом месяце.")
                        }
                    })
                }
                
            } else {
                // review not fileld fully
                ARSLineProgress.showFail()
                self.showAlert(title: "Ошибка", message: "Пожалуйста, заполните все необходимые поля для отзыва.")
            }
    }
    
    @IBAction func recommendButtonClicked(sender: UIButton) {
        sender.setImage(#imageLiteral(resourceName: "recomend-button"), for: .normal)
        notRecommendButton.setImage(#imageLiteral(resourceName: "gray-not-recomend-button"), for: .normal)
        self.review.isRecommend = true
        mainHide()
    }
    
    @IBAction func notRecommendButtonClicked(sender: UIButton) {
        sender.setImage(#imageLiteral(resourceName: "not-recomend-button"), for: .normal)
        recommendButton.setImage(#imageLiteral(resourceName: "gray-recomend-button"), for: .normal)
        self.review.isRecommend = false
        mainHide()
    }
    
    func hideTableView() {
        guard let _ = searchResultsTableView else {
            return
        }
        
        UIView.animate(withDuration: 0.3, animations: {
            self.searchResultsTableView.alpha = 0
        })
    }
    
    func showTableView(textField: UITextField) {
        guard let _ = searchResultsTableView else {
            return
        }
        
        view.bringSubview(toFront: searchResultsTableView)
        
        if keyboardRect != CGRect() {
            self.searchResultsTableView.frame.origin.y = self.activeTextField.frame.maxY + 5
            self.searchResultsTableView.frame.size.height = keyboardRect.minY - self.activeTextField.frame.maxY - 10 - keyboardRect.size.height
            UIView.animate(withDuration: 0.3, animations: {
                self.searchResultsTableView.alpha = 1
            })
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return searchResults.type == .company ? UITableViewAutomaticDimension : 44
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return searchResults.type == .company ? 78 : 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if searchResults.type == .country || searchResults.type == .city || searchResults.type == .address {
            let cell = tableView.dequeueReusableCell(withIdentifier: "oneLabelCell", for: indexPath) as! CountryTableViewCell
            cell.configureCellI(text: searchResults.data[indexPath.row] as! String, textUppercased: true)
            return cell
        } else if searchResults.type == .company {
            
            let companies = searchResults.data as! [Company]
            let company = companies[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "companyCell", for: indexPath) as! CompanyTableViewCell
            cell.configureCell(company: company)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
       if searchResults.type == .company {
            let company = (searchResults.data as! [Company])[indexPath.row]
            adjustViewControllerForCompany(company: company)
        }
        
        mainHide()
    }
    
    func adjustViewControllerToNewReview() {
        let enableColor: UIColor = .white
        addressField.backgroundColor = enableColor
        categoryField.backgroundColor = enableColor
        companyNameField.backgroundColor = enableColor
        
        addressField.isEnabled = true
        categoryField.isEnabled = true
        
        self.review.isCompanySetted = false // it's very important, cause code needs to know (create new company or now) - 1
        self.recommendButton.setImage(#imageLiteral(resourceName: "recomend-button"), for: .normal)
        self.notRecommendButton.setImage(#imageLiteral(resourceName: "not-recomend-button"), for: .normal)
    }
    
    func adjustViewControllerForCompany(company: Company) {
        self.review.address = company.address
        self.review.category = company.category
        self.review.city = company.city
        self.review.country = company.country
        self.review.companyID = company.id
        self.review.companyName = company.name
        
        self.review.isCompanySetted = true // it's very important, cause code needs to know (create new company or now) - 2
        
        addressField.backgroundColor = REVIEW_DISABLE_COLOR
        categoryField.backgroundColor = REVIEW_DISABLE_COLOR
        companyNameField.backgroundColor = REVIEW_DISABLE_COLOR
        
        addressField.isUserInteractionEnabled = false
        categoryField.isUserInteractionEnabled = false
        
        addressField.text = review.address
        categoryField.text = review.category
        companyNameField.text = review.companyName
    }
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.data.count
    }


    func pop(sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    func setLeftBarButton() {
        let barButton = UIBarButtonItem(image: #imageLiteral(resourceName: "iconBackArrow"), style: .plain, target: self, action: #selector(self.pop(sender:)))
        barButton.tintColor = .black
        navigationItem.leftBarButtonItem = barButton

    }
    
    
   
    func addNotificationsForKeyboard() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        delay(delay: 0.2) {
            UIView.animate(withDuration: 0.2, animations: {
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                if self.activeTextField.tag == 2 {
                    self.view.frame.origin.y -= keyboardSize.height - (self.view.frame.height - self.sendReviewButton.frame.maxY) + 15
                }
                
                self.keyboardRect = keyboardSize
            }
          })
        }
    }
   
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        addShadowView()
    }
    
    func addShadowView() {
        let shadowView = UIView(frame: reviewView.frame)
        shadowView.layer.cornerRadius = reviewView.layer.cornerRadius
        shadowView.alpha = 0
        shadowView.embedMediumShadow()
        shadowView.isUserInteractionEnabled = false
        
        view.addSubview(shadowView)
        
        UIView.animate(withDuration: 0.5, animations: {
            shadowView.alpha = 1
        })
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        var isActivated = false
        for subview in view.subviews where subview is UITextField {
            if (subview as! UITextField).isFirstResponder {
                isActivated = true; break
            }
        }
        
        if isActivated && activeTextField == companyNameField {
            adjustViewControllerToNewReview()
        }
        
        mainHide()
    }
    
    func mainHide() {
        resignAllTextFields()
        reviewTextView.resignFirstResponder()
        hideTableView()
    }
  
    func configureReviewTextViewPlaceholder() {
        reviewTextView.text = REVIEW_PLACEHOLDER_TEXT
        reviewTextView.textColor = REVIEW_PLACEHOLDER_COLOR
        reviewTextView.selectedTextRange = reviewTextView.textRange(from: reviewTextView.beginningOfDocument, to: reviewTextView.beginningOfDocument)

    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        navigationController?.show()
        setLeftBarButton()
    }
    
    
    @IBAction func anonimClicked(sender: UIButton) {
        let activated = sender.imageView?.image == #imageLiteral(resourceName: "anonim_active")
        let image = activated ? #imageLiteral(resourceName: "anonim_deactive") : #imageLiteral(resourceName: "anonim_active")
        sender.setImage(image, for: .normal)
        nameField.isEnabled = activated
        nameField.backgroundColor = activated ? .white : UIColor(netHex: 0xf1f1f1)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !selectedLocation.address.isEmpty {
            review.address = selectedLocation.address
            addressField.text = selectedLocation.address
            addressField.setRightImage(image: #imageLiteral(resourceName: "success-field"))
        }
        
        review.coordinate = selectedLocation.coordinate
        
        if !selectedLocation.country.isEmpty {
            review.country = selectedLocation.country
        }
        
        if !selectedLocation.city.isEmpty {
            review.city = selectedLocation.city
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.hide()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func delegating() {
        reviewTextView.delegate = self
        for textField in view.subviews where textField is CircleTextField {
            (textField as! CircleTextField).delegate = self
        }
    }
    
    func resignAllTextFields() {
        for textField in view.subviews where textField is CircleTextField {
            textField.resignFirstResponder()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    
    func initDark() {
        dark = Darkness(alpha: 0.15)
    }
    
    // TEXTFIELD - DELEGATE
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        mainHide()
        adjustViewControllerToNewReview()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let updatedText = (textField.text! as NSString).replacingCharacters(in: range, with: string) as String
        
        if !updatedText.isEmpty {
            switch activeTextField {
                case companyNameField:
                    LCFunc().searchByCompany(name: updatedText, category: categoryField.text, city: review.city, country: review.country, completion: { (companies) in
                        self.searchResults.data = companies
                        self.searchResults.type = .company
                        self.searchResultsTableView.reloadData()
                        self.showTableViewIfNeeded()
                    })
                case addressField:
                    LCFunc().searchByStreet(text: updatedText, completion: { (adresses) in
                        self.searchResults.data = adresses
                        self.searchResults.type = .address
                        self.searchResultsTableView.reloadData()
                        self.showTableViewIfNeeded()
                    })
                
                default: break
            }
            
        } else {
            searchResults.clear()
            hideTableView()
        }
        
        self.showTableViewIfNeeded()
        
        if searchResults.type != .address {
            searchResultsTableView.reloadData()
        }
        
        return true
    }
    
    
    func showTableViewIfNeeded() {
        if searchResults.type == .null || searchResults.data.count == 0 { //hide tableview
            hideTableView()
        } else {
            showTableView(textField: activeTextField)
        }
        
    }
    
    func initTableView() {
        searchResultsTableView = UITableView()
        searchResultsTableView.frame.size = CGSize(width: view.frame.width * 0.8, height: view.frame.height * 0.4)
        searchResultsTableView.center.x = view.center.x
        searchResultsTableView.layer.cornerRadius = 15
        searchResultsTableView.backgroundColor = .white
        searchResultsTableView.embedStandartShadow()
        searchResultsTableView.alpha = 0
        searchResultsTableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 15)
        searchResultsTableView.delegate = self
        searchResultsTableView.dataSource = self
        searchResultsTableView.clipsToBounds = true
        
        searchResultsTableView.register(CountryTableViewCell.self, forCellReuseIdentifier: "oneLabelCell")
        let nib = UINib(nibName: "CompanyTableCellXib", bundle: nil)
        searchResultsTableView.register(nib, forCellReuseIdentifier: "companyCell")
        
        view.addSubview(searchResultsTableView)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField is CircleTextField {
            (textField as! CircleTextField).off()
        }
        
        switch activeTextField {
            case companyNameField:
                review.companyName = textField.text?.lowercased() ?? ""
            case categoryField:
                review.category = textField.text?.lowercased() ?? ""
            case addressField:
                review.address = textField.text?.lowercased() ?? ""
            case nameField:
                review.sender = textField.text!.isEmpty ? "Аноним" : textField.text!
            default: break
        }
        
        if textField.text!.isEmpty { textField.text = textField.text!.capitalized }
        
        dark.hide()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == addressField {
            textField.resignFirstResponder()
            self.transferVCwith(identifier: "selectMap", method: .push)
            return
        }
        
        searchResults.clear()
        
        activeTextField.tag = 1
        if textField is CircleTextField {
            (textField as! CircleTextField).on()
        }
        
        activeTextField = textField
        
        view.addSubview(dark)
        view.bringSubview(toFront: textField)
        dark.show()
    }
    
    // TEXTVIEW - DELEGATE
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        activeTextField.tag = 2
        if textView.textColor == REVIEW_PLACEHOLDER_COLOR {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        UIView.animate(withDuration: 0.2, animations: {
            self.view.frame.origin.y = 0
        })
        
        if textView.text.isEmpty {
            textView.text = REVIEW_PLACEHOLDER_TEXT
            textView.textColor = REVIEW_PLACEHOLDER_COLOR
            review.text = ""
        } else {
            review.text = textView.text!
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text
        let updatedText = (currentText! as NSString).replacingCharacters(in: range, with: text)
        
        if updatedText.isEmpty {
            textView.text = REVIEW_PLACEHOLDER_TEXT
            textView.textColor = REVIEW_PLACEHOLDER_COLOR
            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            
            return false
        } else if textView.textColor == REVIEW_PLACEHOLDER_COLOR && !text.isEmpty {
            textView.text = nil
            textView.textColor = UIColor.black
        }
        
        review.text = updatedText.isEmpty ? "" : updatedText
        
        return updatedText.characters.count < 500
    }
}
