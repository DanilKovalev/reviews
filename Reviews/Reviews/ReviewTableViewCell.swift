//
//  ReviewTableViewCell.swift
//  Reviews
//
//  Created by Данил on 06.05.17.
//  Copyright © 2017 DevingLabs. All rights reserved.
//

import UIKit

class ReviewTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var reviewTextView: UITextView!
    @IBOutlet weak var showFullButton: UIButton!
    @IBOutlet weak var statusIcon: UIImageView!
    @IBOutlet weak var backgroundStateView: UIView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    var full = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundStateView.layer.cornerRadius = 15
        backgroundStateView.layer.shadowColor = UIColor.black.cgColor
        backgroundStateView.layer.shadowOpacity = 0.2
        backgroundStateView.layer.shadowOffset = CGSize.zero
        backgroundStateView.layer.shadowRadius = 1.8
        // Initialization code
    }
    
    func configure(review: CompanyReview) {
        nameLabel.text = review.sender
        dateLabel.text = Date(timeIntervalSince1970: TimeInterval(review.unix)).convertFormateToNormDateString(format: "dd.MM.yyyy")
        statusIcon.image = review.isRecommend ? #imageLiteral(resourceName: "like") : #imageLiteral(resourceName: "dislike")
        reviewTextView.text = review.text
        
        showFullButton.isHidden = !(review.text.characters.count > 110) || full
        
        if let _ = heightConstraint {
            let count = review.text.characters.count
            if count >= 0 && count <= 44 {
                heightConstraint.constant = 35
            } else {
               heightConstraint.constant = 80
            }
        } else {
            showFullButton.isHidden = true
            // если heightConstraint не существует
        }
        
        full = showFullButton.isHidden
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        reviewTextView.font = UIFont(name: "Calibri", size: 17.0)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
